﻿using GraphQL;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PIMSPE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public static class ExtensionMethods
    {

        private static Action EmptyDelegate = delegate () { };


        public static void Refresh(this UIElement uiElement)
        {
            uiElement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }
    }
    public partial class MainWindow : Window , INotifyPropertyChanged
    {

        private BackgroundWorker _bgwork = new BackgroundWorker();
        private int _workerState;
         int ilezrobilem=0;
        public int ile
        {
            get { return ilezrobilem; }
            set
            {
                ilezrobilem = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ilezrobilem"));
            
            }
        }

        public int workerstate
        {
            get { return _workerState; }
            set
            {
                _workerState = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("workerstate"));
            }
        }
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private string sss = Directory.GetCurrentDirectory();
        public event PropertyChangedEventHandler PropertyChanged;

        public MainWindow()
        {

            DateTime thisDay = DateTime.Now;
            var config = new NLog.Config.LoggingConfiguration();

            // Targets where to log to: File and Console
            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = @"log\" + "log" + thisDay + ".txt" };

            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");

            // Rules for mapping loggers to targets            
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logconsole);
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, logfile);
            NLog.LogManager.Configuration = config;
         
            InitializeComponent();
            //    Adding();

            tekst.Visibility = Visibility.Hidden; 
            PP.Visibility = Visibility.Hidden;
            Content.Visibility= Visibility.Hidden;
            IleZ.Visibility= Visibility.Hidden;
            if (Properties.Settings.Default.Autogeneracja_Autozamkniecie == true)
            {

            }

            log oo = new log();
   //         Adding();
            
          //  tekst.Text = ilezrobilem.ToString();


        }


        public Root LoadJson(string path)
        {
            using (StreamReader r = new StreamReader(path+"\\data.txt"))
            {
                string json = r.ReadToEnd();
                Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(json);
                return myDeserializedClass;
            }
        }
        public  void Adding(string katalog)
        {
            List<string> lis = new List<string>();

        
            ProcessStartInfo process = new ProcessStartInfo();
            process.CreateNoWindow = false;
            process.UseShellExecute = true;
            process.FileName = katalog+"\\main\\main.exe";
         //   process.Verb = "runas";

            // string katalog = 
                    process.WindowStyle = ProcessWindowStyle.Hidden;
            using (Process exeProcess = Process.Start(process))
            {
                exeProcess.WaitForExit();
            }
            string pa = "C:\\dane_pim";
            var rooss=       LoadJson(pa);


         

            //var heroRequest = new GraphQLRequest
            //{












            //              query = """{

            // getProductListing(first: 1000, after: 0, filter: "{\\"getFolder\\": {\\"$like\\" :\\" / Brands /%\\"}, \\"isParent\\": {\\"$like\\" : \\"child\\"}}") {



            //    string testtt = 




            //getFolder: {$like :/Brands/%}, isParent: {$like : child }
            //   Query = $@"{{getProductListing(first: {Properties.Settings.Default.ILE_TOWAROW}, after: 0, filter:\getFolder\:\$like\:\/ Brands /%\, \isParent\:\$like\:\child\ )  {{
            //         Query = $@"{{getProductListing(first: {Properties.Settings.Default.ILE_TOWAROW}, after: 120,filter:{{\\getFolder\\:$like\\:\\/Brands/%\\, \\isParent\\:{{\\$like}}) {{


            //    edges {{
            //      node {{
            //        besoLuxRef
            //        producerRel
            //        brandRel
            //        titleForLabels
            //        producersReference
            //        producersPrice
            //        ean
            //        packagesQuantity
            //        fColor
            //        numberOfSeats
            //        fabrics {{
            //          ...on object_Fabric
            //        {{
            //            series
            //        }}
            //    }}
            //    legs {{
            //          ... on object_Leg
            //    {{
            //        color
            //            height
            //            reference
            //    }}
            //}}

            //totalWeight
            //totalVolume
            //        category
            //        family
            //        hsCode
            //        legsPlacement
            //        legsNumber
            //      }}
            //    }}
            //  }}

            //}}"

            //            };

            ////

            //         string sss = ss.Data.ToString();
            //   var sd= ss.AsGraphQLHttpResponse<GetProductListing>();

            //var ssss = ss.Data.ToObject<Root>();

            foreach (var data in rooss.data.getProductListing.edges)
            {
                string atrfeetcolor = "";
                string atrfeetref = "";
                int atrfeetheight = 0;
                log opt = new log();
                string kod = data.node.besoLuxRef.EmptyIfNull();
                string nazwa = data.node.besoLuxRef.EmptyIfNull();
                string producent = data.node.producersReference.EmptyIfNull();
                string marka = data.node.brandRel.EmptyIfNull();
                string dostawca = data.node.producerRel.EmptyIfNull();
                string ean = data.node.ean.EmptyIfNull();
                string koddst = data.node.producersReference.EmptyIfNull();
                string opis = data.node.titleForLabels.EmptyIfNull();
                string cenaz = data.node.producersPrice.EmptyIfNull();
                //
                string ilosc = data.node.totalVolume.EmptyIfNull();
                string waga = data.node.totalWeight.EmptyIfNull();
                string category = data.node.category.EmptyIfNull();
                string koddostawcy = data.node.producerRel.EmptyIfNull();
                //  opt.Add(kod, opis, marka,category);
                string cena = data.node.producersPrice.EmptyIfNull();
                string atrcolor = data.node.fColor.EmptyIfNull();
                string atrSEATS = data.node.numberOfSeats.EmptyIfNull();
                string atrfabric = data.node.fabrics.EmptyIfNull();
                string atrfamily = data.node.family.EmptyIfNull();
                string atrhscode = data.node.hsCode.EmptyIfNull();
                string atrfeetplm = data.node.legsPlacement.EmptyIfNull();
                string atrfeetnumber = data.node.legsNumber.EmptyIfNull();
                if (data.node.legs != null)
                {
                    foreach (var a in data.node.legs)
                    {
                        atrfeetcolor = a.color.EmptyIfNull();
                        atrfeetref = a.reference.EmptyIfNull();
                        atrfeetheight = a.height;
                    }

                }
                else
                {
                    atrfeetcolor = "brak";
                    atrfeetref = "brak";
                    atrfeetheight = 1;
                }

                log o = new log();
                logger.Info("Dodaje karte: \nKOD: {0}\n KATEGORIA: {1}\n KOD DOSTAWCY:{2}\n CENA:{3}\n MARKA:{4}\n WAGA:{5}\n ATRCOLOR={6} ATRREF={7} ATRHEIG={8}\n ILOSC:{9}\n EAN:{10}", kod, category, koddostawcy, cena, marka, waga, atrfeetcolor, atrfeetref, atrfeetheight, ilosc, ean);
                //Logger.info("Dodaje karte : \n Kod = {0}  ),






                var addingg = o.Add(kod,nazwa, opis, marka, category, koddst, koddst, cena, atrfeetcolor, atrfeetref, atrfeetheight, waga, ilosc, ean, ilosc, atrcolor, atrSEATS, atrfabric, atrfamily, atrhscode, atrfeetplm, atrfeetnumber);
                //  return lis;
                if (addingg == true)
                {
                    ilezrobilem++;
                    tekst.Text = ilezrobilem.ToString();

                }
                else
                {

                    ilezrobilem++;
                    tekst.Text = ilezrobilem.ToString();

                }
                tekst.Refresh();
            }


        }


     

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            tekst.Visibility = Visibility.Visible;
            PP.Visibility = Visibility.Visible;
            Content.Visibility = Visibility.Visible;
            IleZ.Visibility = Visibility.Visible;
            IleZ.Text = Properties.Settings.Default.ILE_TOWAROW.ToString();
            tekst.Text = ilezrobilem.ToString();
            if (loginbox.Text.Length == 0)
            {
                MessageBox.Show("Wpisz login i hasło");
            }

            else
            {
                log oo = new log();
                var log =oo.LogowanieAutomatyczne(loginbox.Text.ToString(),Passbox.Password);
                if (log == false)
                {
                    MessageBox.Show("Błąd logowania do optimy!");
                }
                else
                {
                


                    Adding(sss);
                    
                   
                }

            }

        //    log oo = new log();
    
      //   Adding();
      
        }
    }
}
