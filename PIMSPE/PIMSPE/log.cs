﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CDNBase;
using CDNTwrb1;
using NLog;
using OP_Twrb2Lib;

namespace PIMSPE
{

    public class log
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        protected static Application Application = null;
        protected static ILogin Login = null;

        public bool LogowanieAutomatyczne(string login , string password)
        {
            string Operator =login;
            string Haslo = password;
            string Firma = Properties.Settings.Default.Optima_Baza;

            object[] hPar = new object[]
            {
                0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0
            }; // do jakich modułów będzie logowanie
            /* Kolejno: KP, KH, KHP, ST, FA, MAG, PK, PKXL, CRM, ANL, DET, BIU, SRW, ODB, KB, KBP, HAP  
             */

            // katalog, gdzie jest zainstalowana Optima (bez ustawienia tej zmiennej nie zadziała, chyba że program odpalimy z katalogu O!)
            System.Environment.CurrentDirectory = @"C:\Program Files (x86)\Comarch ERP Optima";

            // tworzenie obiektu apliakcji
            Application = new Application();
            // blokowanie
            Application.LockApp(513, 5000, null, null, null, null);
            // logowanie do podanej Firmy, na danego operatora, do podanych modułów

            try
            {
                Console.WriteLine("Logowanie...");
                Login = Application.Login(Operator, Haslo, Firma, hPar[0], hPar[1], hPar[2], hPar[3], hPar[4], hPar[5],
                    hPar[6], hPar[7], hPar[8], hPar[9], hPar[10], hPar[11], hPar[12], hPar[13], hPar[14], hPar[15],
                    hPar[16]);
                Console.WriteLine("Prawidowe zalogowanie");
                return true;

            }
            catch
            {
                Console.WriteLine("Logowanie zakończono niepowodzeniem.");
                Application.UnlockApp();
                return false;
            }

            // tu jestemy zalogowani do O!            
        }

        public void Wylogowanie()
        {
            // niszczymy Login
            Login = null;
            // odblokowanie (wylogowanie) O!
            Application.UnlockApp();
            // niszczymy obiekt Aplikacji
            Application = null;
        }

        public bool Add(string kod,string nazwa, string opis, string marka, string category, string dostawca,string koddostawcy,string price,string atrcolor, string atrreference, int atrheight,string waga, string ilosc,string ean,string ATR_PARCELS, string atrcolor1,string atrseats, string atrfabric, string atrfam, string atrhscode, string atrfeetplm, string atrfeetnumber)
        {
        //    bool truuu = LogowanieAutomatyczne();
            try
            {

                CDNBase.AdoSession Sesja = Login.CreateSession();

                var towary = (CDNTwrb1.Towary) Sesja.CreateObject("CDN.Towary", null);
                var towar = (CDNTwrb1.ITowar) towary.AddNew(null);

                try
                {


                 //   var grupy = (CDNBase.ICollection) (Sesja.CreateObject("CDN.TwrGrupy", null));
                  //  var grupa = (CDNTwrb1.TwrGrupa) grupy["twg_kod ='" + category + "'"];
                   // towar.TwGGIDNumer = grupa.GIDNumer;



                }
                catch (Exception ee)
                {
                    logger.Error(ee);



                }

        
              
                towar.Nazwa = nazwa;
                towar.Kod = kod;
                towar.Stawka = 23.00m;
                 towar.EAN = ean;
                towar.Flaga = 2;
                towar.KodDostawcy = koddostawcy;
                towar.Opis = opis;
           //     towar.WagaKG = Convert.ToDouble(waga);
                towar.JM = "SZT";
          //      towar.IloscZam = Convert.ToDouble(ilosc);
                //   towar.WagaKG = waga;

                // towar.URL = Url;


      
                //TwA_DeAId



                var pr = (CDNBase.ICollection) (Sesja.CreateObject("CDN.Marki", null));
                try
                {
                    var pr1 = (OP_Twrb2Lib.Marka) pr["Mrk_Nazwa ='" + marka + "'"];
                    towar.MrkID = pr1.ID;

                }
                catch
                {
                    var pr1 = (OP_Twrb2Lib.Marka) pr.AddNew();
                    pr1.Nazwa = marka;

                    towar.MrkID = pr1.ID;
                }

                if (price.Length > 0)
                {
                   foreach (ICena cena in towar.Ceny)
                   {
                        if (cena.Numer == 1)
                        {
                            cena.WartoscNetto = Convert.ToDecimal(price);

                        }
                    }
                }
                else
                {
                    logger.Error("Cena zerowa dla towaru {0}",kod);
                }
                Sesja.Save();
                var atry = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));
                //var at = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));
                var at1 = (IDefAtrybut)atry["DeA_Kod ='" + Properties.Settings.Default.ATR_Color + "'"];
                 
                var atrybut1 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                //35
                atrybut1.DeAID = at1.ID;
                
      
                atrybut1.Wartosc = atrcolor;

                Sesja.Save();
                var atry1 = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));

                var at2 = (IDefAtrybut)atry1["DeA_Kod ='" + Properties.Settings.Default.ATR_height + "'"];
                var atrybut2 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                //35
                atrybut2.DeAID = at2.ID;
    
                atrybut2.Wartosc = atrheight.ToString();

                Sesja.Save();
                var atry2 = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));

                var at2222 = (IDefAtrybut)atry2["DeA_Kod ='" + Properties.Settings.Default.ATR_reference + "'"];
               
                var atrybut3 = (ITwrAtrybut)towar.Atrybuty.AddNew();

                //35
                atrybut3.DeAID = at2222.ID;
           
                atrybut3.Wartosc = atrreference;

                Sesja.Save();
                //////////////
                ///
                var atry33 = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));

                var atpar1 = (IDefAtrybut)atry33["DeA_Kod ='" + Properties.Settings.Default.ATR_packagesQuantity + "'"];

                var atrybut4 = (ITwrAtrybut)towar.Atrybuty.AddNew();

                
                atrybut4.DeAID = atpar1.ID;

                atrybut4.Wartosc = atrreference.ToString();

                Sesja.Save();
                ///////////////////////
                ///                var atry = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));
                var atry4 = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));

                var atcolorr = (IDefAtrybut)atry4["DeA_Kod ='" + Properties.Settings.Default.ATR_fColor + "'"];
                var atcolor = (ITwrAtrybut)towar.Atrybuty.AddNew();
                atcolor.DeAID = atcolorr.ID;
                atcolor.Wartosc = atrcolor1.ToString();
                 Sesja.Save();


                ///////////////////////
                ///                var atry = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));
                var atry55 = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));

                var atseats = (IDefAtrybut)atry55["DeA_Kod ='" + Properties.Settings.Default.ATR_numberOfSeats + "'"];
                var atsea = (ITwrAtrybut)towar.Atrybuty.AddNew();
                atsea.DeAID = atseats.ID;
                atsea.Wartosc = atrseats.ToString();
                Sesja.Save();
                ///////////////////////
                ///                var atry = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));
                var atry6 = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));

                var atfab = (IDefAtrybut)atry6["DeA_Kod ='" + Properties.Settings.Default.ATR_series + "'"];
                var atfabr = (ITwrAtrybut)towar.Atrybuty.AddNew();
                atfabr.DeAID = atfab.ID;
                atfabr.Wartosc = atrfabric.ToString();
                Sesja.Save();

                ///////////////////////
                ///                var atry = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));
                var atry7 = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));

                var atwaga = (IDefAtrybut)atry7["DeA_Kod ='" + Properties.Settings.Default.ATR_totalWeight + "'"];
                var atwa = (ITwrAtrybut)towar.Atrybuty.AddNew();
                atwa.DeAID = atwaga.ID;
                atwa.Wartosc = waga.ToString();
                Sesja.Save();
                ///////////////////////
                ///                var atry = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));
                var atry8 = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));

                var atFAMILY = (IDefAtrybut)atry8["DeA_Kod ='" + Properties.Settings.Default.ATR_family + "'"];
                var atFAMILY1 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                atFAMILY1.DeAID = atFAMILY.ID;
                atFAMILY1.Wartosc = atrfam.ToString();
                Sesja.Save();

                ///////////////////////
                ///                var atry = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));
                var atry88 = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));

                var athscode = (IDefAtrybut)atry88["DeA_Kod ='" + Properties.Settings.Default.ATR_hsCode + "'"];
                var athscode1 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                athscode1.DeAID = athscode.ID;
                athscode1.Wartosc = atrhscode.ToString();
                Sesja.Save();

                ///////////////////////
                ///
                var atry99 = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));

                var atatrfeetplm = (IDefAtrybut)atry99["DeA_Kod ='" + Properties.Settings.Default.ATR_legsPlacement + "'"];
                var atrfeetplm1 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                atrfeetplm1.DeAID = atatrfeetplm.ID;
                atrfeetplm1.Wartosc = atrfeetplm.ToString();
                Sesja.Save();
                ///////////////////////
                ///                var atry = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));
                var atry10 = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));

                var atrfeetnumber12 = (IDefAtrybut)atry10["DeA_Kod ='" + Properties.Settings.Default.ATR_legsNumber + "'"];
                var atrfeetnumber1 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                atrfeetnumber1.DeAID = atrfeetnumber12.ID;
                atrfeetnumber1.Wartosc = atrfeetnumber.ToString();
                Sesja.Save();
                var atry11 = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));

                ///////////////////////
                var atrcategory = (IDefAtrybut)atry11["DeA_Kod ='" + Properties.Settings.Default.ATR_category + "'"];
                var atrcategory1 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                atrcategory1.DeAID = atrcategory.ID;
                atrcategory1.Wartosc =category.ToString();
                Sesja.Save();
                var atry12 = (CDNBase.ICollection)(Sesja.CreateObject("CDN.DefAtrybuty", null));
                var atrvolume = (IDefAtrybut)atry12["DeA_Kod ='" + Properties.Settings.Default.ATR_totalVolume + "'"];
                var atrvolumen1 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                atrvolumen1.DeAID = atrvolume.ID;
                atrvolumen1.Wartosc = ilosc.ToString();
                Sesja.Save();



                //ATR_totalVolume
                return true;


            }





            catch (Exception e)

            {
                logger.Info(e);
                return false;
         
                //GetSql sql = new GetSql();
                //var ids = sql.CheckTwrID(kod);
                //foreach (var id in ids)
                //{
                //  sql.UpdateQty(id.TWRXML_ID, Qty);
                // }
                //logger.Error("Towar o kodzie {0} juz istnieje. Aktualizuje ilosc na:{1}", kod, Qty);



            }
        }
    }

}


