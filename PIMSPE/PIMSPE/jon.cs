﻿// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
using System.Collections.Generic;

public class Fabric
{
    public string series { get; set; }
}

public class Leg
{
    public string color { get; set; }
    public int height { get; set; }
    public string reference { get; set; }
}

public class Node
{
    public string besoLuxRef { get; set; }
    public string producerRel { get; set; }
    public string brandRel { get; set; }
    public string titleForLabels { get; set; }
    public string producersReference { get; set; }
    public double producersPrice { get; set; }
    public object ean { get; set; }
    public string packagesQuantity { get; set; }
    public string fColor { get; set; }
    public string numberOfSeats { get; set; }
    public List<Fabric> fabrics { get; set; }
    public List<Leg> legs { get; set; }
    public string totalWeight { get; set; }
    public string totalVolume { get; set; }
    public string category { get; set; }
    public string family { get; set; }
    public object hsCode { get; set; }
    public string legsPlacement { get; set; }
    public int legsNumber { get; set; }
}

public class Edge
{
    public Node node { get; set; }
}

public class GetProductListing
{
    public List<Edge> edges { get; set; }
}

public class Data
{
    public GetProductListing getProductListing { get; set; }
}

public class Root
{
    public Data data { get; set; }
}

