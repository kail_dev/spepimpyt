import os

import requests
import json
request_url = 'http://pim.besolux.com/pimcore-graphql-webservices/optima?apikey=c055fd02a5c31d6e7c3e2276741ef177'

query = """{
  getProductListing(first: 1000, after: 0, filter: "{\\"getFolder\\": {\\"$like\\" :\\"/Brands/%\\"}, \\"isParent\\": {\\"$like\\" : \\"child\\"}}") {
    edges {
      node {
        besoLuxRef
        producerRel
        brandRel
        titleForLabels
        producersReference
        producersPrice
        ean
        packagesQuantity
        fColor
        numberOfSeats
        fabrics {
          ...on object_Fabric {
            series
          }
        }
        legs {
          ... on object_Leg {
            color
            height
            reference
          }
        }
        totalWeight
        totalVolume
        category
        family
        hsCode
        legsPlacement
        legsNumber
      }
    }
  }
}
"""

r = requests.post(request_url, json={'query': query})
json_data = json.loads(r.text)
save_path = 'C:\\dane_pim'

file_name = "data.txt"
completeName = os.path.join(save_path, file_name)
with open(completeName, 'w') as txtfile:
    json.dump(json_data, txtfile)